import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  cities = ['warsaw', 'larnaca', 'madrid', 'moscow', 'helsinki']; // create an array with cities, and loop over it in the template
}
