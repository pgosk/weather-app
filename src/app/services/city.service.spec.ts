import { TestBed } from '@angular/core/testing';

import { CityService } from './city.service';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { inject } from '@angular/core';

describe('CityService', () => {
  let service: CityService;

  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [HttpClient, HttpHandler] });
    service = TestBed.inject(CityService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return response', () => {
    const cityName = 'Warsaw';
    service.fetchCityWeather(cityName).subscribe(({ name }) => {
      expect(name).toEqual('Warsaw');
    });
  });

  it('should return fail', () => {
    const cityName = 'Warsaw1111';
    service.fetchCityWeather(cityName).subscribe((response) => {
      expect(response).toThrowError();
    });
  });
});
