import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { catchError, map } from 'rxjs/operators';
import { EMPTY } from 'rxjs';

/* I defined in interfaces only data which I needed */

interface WeatherResponseData {
  name: string;
  wind: {
    speed: number;
  };
  coord: {
    lon: number;
    lat: number;
  };
  main: {
    temp: number;
  };
}

interface ForecastResponseData {
  hourly: Array<{
    dt: number;
    temp: number;
  }>;
}

// Build a url for service
const setUrl = (endpoint: string): string =>
  `//api.openweathermap.org/data/2.5/${endpoint}&appid=${environment.apiKey}&units=${environment.units}`;

@Injectable({
  providedIn: 'root',
})
export class CityService {
  constructor(private http: HttpClient) {}

  // Get weather data depending on city name
  fetchCityWeather(city: string) {
    return this.http.get<WeatherResponseData>(setUrl(`weather?q=${city}`)).pipe(
      map(({ name, wind, main, coord }) => {
        return {
          name,
          wind: +wind.speed.toFixed(1),
          temperature: +main.temp.toFixed(0),
          coord,
        };
      }),
      catchError(() => EMPTY)
    );
  }

  // Get forecast for city
  fetchForecastWeather(lon: number, lat: number) {
    return this.http
      .get<ForecastResponseData>(setUrl(`onecall?lat=${lat}&lon=${lon}`))
      .pipe(
        map((response) => {
          response.hourly.length = 10; // Cut hours for only next 10
          return response.hourly.map(({ dt, temp }) => ({
            time: new Date(dt * 1000),
            temperature: +temp.toFixed(0),
          }));
        }),
        catchError(() => EMPTY)
      );
  }
}
