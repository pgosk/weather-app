import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-forecast',
  template: `<div>
    <strong>{{ temperature }} &deg;C</strong><br />
    {{ day | date: 'hh:mm' }}
  </div>`,
})
export class ForecastComponent {
  @Input() day: Date;
  @Input() temperature: number;
}
