import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CityComponent } from './city.component';
import { CityService } from '../../services/city.service';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('CityComponent', () => {
  let component: CityComponent;
  let fixture: ComponentFixture<CityComponent>;
  let h2: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CityComponent],
      providers: [CityService, HttpClient, HttpHandler],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CityComponent);
    component = fixture.componentInstance;
    h2 = fixture.nativeElement.querySelectorAll('h2');
    console.log(fixture);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should have value for name props`, () => {
    fixture = TestBed.createComponent(CityComponent);
    component = fixture.componentInstance;
    component.name = 'City name';
    fixture.detectChanges();
    expect(component.name).toEqual('City name');
  });
});
