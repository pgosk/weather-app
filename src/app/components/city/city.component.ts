import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { CityService } from '../../services/city.service';
import { Subscription } from 'rxjs';

export interface City {
  name: string;
  wind: number;
  temperature: number;
  coord: {
    // I need it to get forecast weather
    lon: number;
    lat: number;
  };
}

interface Forecast {
  time: Date;
  temperature: number;
}

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.scss'],
})
export class CityComponent implements OnInit, OnDestroy {
  @Input() name: string; // retrieve name of city

  data: City;
  forecast: Forecast[] = []; // store forecast weather for next 10 hours
  citySubscription: Subscription;
  forecastSubscription: Subscription;
  showForecast: boolean;

  constructor(private cityService: CityService) {}

  ngOnInit(): void {
    // Fetch data from API on component init
    this.citySubscription = this.cityService
      .fetchCityWeather(this.name)
      .subscribe((data) => {
        this.data = data;
      });
  }

  ngOnDestroy(): void {
    // Remove subscriptions on component destroy
    this.citySubscription.unsubscribe();
    if (this.forecastSubscription) {
      this.forecastSubscription.unsubscribe();
    }
  }

  // Set background for city component depends of temperature
  setBackground(temperature: number = 1): object {
    /*
    Initial hue = 180 is a 0C
    Multiple temperature by -6 to get correct hue colors
    Multiplied by minus to set correct hue color 'direction' - warm will be green then yellow, cold will be blue, then purpury
    I use 6 to get a +/- 30 C temperature range
     */
    const hue = 180 + temperature * -6;
    return {
      background: `hsl(${hue}, 90%, 90%)`,
    };
  }

  // Fetch forecast
  getForecast(): void {
    const { lon, lat } = this.data.coord;
    this.forecastSubscription = this.cityService
      .fetchForecastWeather(lon, lat) // endpoint requires coordinates instead of name
      .subscribe((res) => {
        this.forecast = res;
      });
  }

  // toggle forecast sections
  toggleForecast(): void {
    this.showForecast = !this.showForecast;
    if (!!this.forecast) {
      // If there is no loaded forecast, do it
      this.getForecast();
    }
  }
}
