# WeatherApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.7.

## Description

I have made a simple application. My first idea was to use NgRx, but after a moment of reflection I thought that will be overcomplicated for such a simple app.

Finally, I use simple service in each CityComponent that retrieve data from OpenWeather, for my cities. 
I just only get what I need from this endpoint.
Cities are defined in an array in AppComponent and then iterate in the template.

By clicking on a city element you get an info about forecast weather for first 10 hours.

I find out that I had to use another api endpoint for that. Where I pass coordinates for current city.

I think that if I could have more hours I had made it better. In my opinion working mvp app is better than nothing.

## TODO

Below I put some features that could be included to be more fancy

- First, code coverage for testing (I started making test but not finish)
- I render `{temperature} deg C`, instead of there could be a pipe
- Endpoints are called with `metric` flag - user could have possibilities to change it
- UI with animations
- Handle errors

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
